package org.tio.showcase;


public class UserServer {

    public int tokenGetUserId(String token) {
        return Integer.valueOf(token);
    }

    public String tokenGetUserIdByString(String token) {
        return token;
    }

    public String getTwoUserGroupName(Integer userIdA, Integer userIdB) {
        if (userIdA > userIdB) {
            return "chat." + String.valueOf(userIdB) + "_" + String.valueOf(userIdA);
        } else {
            return "chat." + String.valueOf(userIdA) + "_" + String.valueOf(userIdB);
        }

    }

}
